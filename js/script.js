$(function(){
	$('select').select2({
		minimumResultsForSearch: 20
	});
	
	$('#mobile-nav').on('change', function(e){
		window.location.href = e.val;
	});
	
    $(':input').each(function(){
        var $this = $(this);
        var label = $this.data('label');
        var wmClass = 'watermarked';
        
        if(typeof label != 'undefined'){
            if($this.val() == ''){
                $this
                    .val(label)
                    .addClass(wmClass);
            }
            
            $this.focus(function(){
                if($this.val() == label){
                    $this
                        .val('')
                        .removeClass(wmClass);
                }
            });
            
            $this.blur(function(){
                if($this.val() == ''){
                    $this
                        .val(label)
                        .addClass(wmClass);
                }
            });
        }
    });
});

(function($){
    $.fn.equalHeight = function(){
        var max = 0;
        
        this.height('');
        
        this.each(function(){
            max = Math.max(max, $(this).height());
        });
        
        this.height(max);
    }
})(jQuery);