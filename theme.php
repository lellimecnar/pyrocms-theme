<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Theme_Blank extends Theme {

    public $name            = 'Blank Theme';
    public $author          = 'Lance Miller';
    public $author_website  = 'http://lellimecnar.com/';
    public $website         = 'http://lellimecnar.com/';
    public $description     = 'An HTML5 base template using SASS, Compass, Twitter Bootstrap, HTML5Shiv, and jQuery.';
    public $version         = '1.0';
    
}

/* End of file theme.php */